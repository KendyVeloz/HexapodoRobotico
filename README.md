En el siguiente trabajo se realiza el diseño e implementación de un robot hexápodo el cual se podrá utilizar para la exploración de terrenos de difícil y peligroso acceso para 
un ser humano, también se podrá utilizar para el entretenimiento.
Para el diseño del robot se utilizara material de fácil manipulación y resistente como lo es el Sintra.
Para la parte de control se ha optado por las placas Arduino que integran un microcontrolador, así como un entorno de desarrollo. 
De esta forma, al implementar un programa no muy complicado, se puede controlar los motores desde una aplicación móvil, 
la cual se comunicara con la placa mediante el módulo bluetooth que se conectara al arduino y a la aplicación móvil. 
A lo largo del proyecto se detallarán los distintos componentes utilizados, el diseño de cada una de las configuraciones y la programación del software.
